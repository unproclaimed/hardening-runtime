#!/bin/sh

set -e

grep -v '^#' < /usr/share/doc/hardening-runtime/examples/package-hardening/statoverrides | grep -v '^$' |
    while read user group mode path; do
	dpkg-statoverride --update --add "$user" "$group" "$mode" "$path" || true
    done

exit 0
